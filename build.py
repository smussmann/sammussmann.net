#! /usr/bin/env python

import cmd
import os
import os.path
import re
import shutil
import socket

import cherrypy
from jinja2 import Environment, FileSystemLoader
from scss import parser

def build_style():
  scss = re.compile(r'.*scss$')
  css = re.compile(r'.*\.css$')
  for dirpath, _, files in os.walk('style'):
    output_path = 'out/' + dirpath
    for file in files:
      if scss.match(file):
        print 'Generating CSS from:', os.path.join(dirpath, file)
        if not os.path.isdir(output_path + '/'):
          os.makedirs(output_path + '/')
        with open(output_path + '/' + file[:-4] + 'css', 'w') as f:
          f.write(parser.load(dirpath + '/' + file))
      elif css.match(file):
        print 'Generating CSS from:', os.path.join(dirpath, file)
        if not os.path.isdir(output_path + '/'):
          os.makedirs(output_path + '/')
        shutil.copy(dirpath + '/' + file, output_path + '/')

def build_static():
  if not os.path.islink('out/static'):
    os.symlink('../static', './out/static')


def build_html():
  content = re.compile(r'.*html$')
  env = Environment(loader=FileSystemLoader('templates'))
  for dirpath, _, files in os.walk('content'):
    output_path = 'out' + dirpath[7:]
    for file in files:
      if content.match(file):
        print 'Generating HTML from:', os.path.join(dirpath, file)
        if not os.path.isdir(output_path):
          os.makedirs(output_path)
        with open(os.path.join(output_path, file), 'w') as f:
          f.write(
              env.from_string(open(os.path.join(dirpath, file)).read()).render())

class Interpreter(cmd.Cmd):
  def do_build(self, line):
    build_style()
    build_static()
    build_html()

  def do_quit(self, line):
    return True

  def do_EOF(self, line):
    return True

class Root(object):
  pass

# Serve site
def serve_site():
  # Get IP address
  s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  s.connect(('google.com', 0))
  ip = s.getsockname()[0]
  PORT = 8000
  cherrypy.config.update({'server.socket_host': '0.0.0.0',
                          'server.socket_port': PORT,
                          'log.screen': False,
                          })
  static_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'out')
  conf = {'/': {'tools.staticdir.on': True,
                'tools.staticdir.dir': static_dir,
                'tools.staticdir.index': 'index.html',
               }}
  cherrypy.tree.mount(Root(), '/', config=conf)
  # Start HTTP server
  print "Serving at http://%s:%d/" % (ip, PORT)
  cherrypy.engine.start()

def main():
  build_style()
  build_static()
  build_html()
  serve_site()
  Interpreter().cmdloop()
  cherrypy.engine.stop()
  cherrypy.engine.exit()

if __name__ == '__main__':
  main()

